import {Component} from '@angular/core';

@Component({
  selector: 'app',
  template:
    `
      <ul>
        <li><a [routerLink]="['/']">home</a></li>
        <li><a [routerLink]="['/items']">items</a></li>
        <li><a [routerLink]="['/heroes']">heroes</a></li>
        <li><a [routerLink]="['/admin/hero/new']">create hero</a></li>
      </ul>
      <router-outlet></router-outlet>
    `,
})
export class AppComponent {
  title = 'D2!';
}
