import {Injectable} from "@angular/core";
import {IItemProperties} from "../models/item-properties.model";
import {Item, IItem} from "../models/item.model";

const ITEMS:IItemProperties[] = [
  {
    name: "Circlet",
    strength: 2,
    agillity: 2,
    intelligence: 2,
    price: 165
  },{
    name: "Ogre Club",
    strength: 10,
    price: 1000
  }, {
    name: "Ring of Protection",
    armor: 2,
    price: 175
  }, {
    name: "Blades of Attack",
    price: 420,
    damage: 9
  }, {
    name: "Vitality Booster",
    price: 1100,
    health_point: 250
  }, {
    name: "Energy Booster",
    price: 900,
    mana_point: 250
  }, {
    name: "Boots of Speed",
    price: 400,
    move_speed: 45
  }, {
    name: "Gloves of Haste",
    price: 500,
    attack_speed: 20
  }, {
    name: "Ring of Regen",
    price: 325,
    health_point_regeneration: 2
  }, {
    name: "Sage's Mask",
    price: 325,
    mana_point_regeneration_percent: 0.5
  }, {
    name: "Infused Raindrop",
    price: 225,
    mana_point_regeneration_integer: 0.85
  }, {
    name: "Cloak",
    price: 550,
    magic_resistance: 0.15
  }
];

@Injectable()
export class ItemService {
  getItems():Promise<IItem[]> {
    if (!this.items) {
      this.items = ITEMS.map((prop:IItemProperties) => new Item(prop));
    }
    return Promise.resolve(this.items);
  }
  getItem(id:number):Promise<IItem> {
    return Promise.resolve(this.items.find((item:IItem) => item.getId() === id));
  }

  private items:Item[];
}
