import {HERO_MAIN_ATTRIBUTE, IHeroProperties} from "../models/hero/hero-properties.model";
import {Injectable} from "@angular/core";
import {IBaseHero} from "../models/hero/base-hero.model";
import {Hero, IHero} from "../models/hero/hero.model";

const HEROES:IHeroProperties[] = [
  {
    name: "Axe",
    strength: 25,
    extra_strength_per_level: 2.5,
    agillity: 20,
    extra_agillity_per_level: 2.2,
    intelligence: 18,
    extra_intelligence_per_level: 1.6,
    mana_point: 50,
    mana_regeneration: 0.01,
    health_point: 200,
    health_regeneration: 3,
    armor: -1,
    min_damage: 24,
    max_damage: 28,
    main_attribute: HERO_MAIN_ATTRIBUTE.STRENGTH,
    base_attack_time: 1.7,
    base_magic_resistance: 0.25,
    base_movement_speed: 290
  },
  {
    name: "Mirana",
    strength: 17,
    extra_strength_per_level: 1.85,
    agillity: 20,
    extra_agillity_per_level: 3.3,
    intelligence: 17,
    extra_intelligence_per_level: 1.65,
    mana_point: 50,
    mana_regeneration: 0.01,
    health_point: 200,
    health_regeneration: 0.25,
    armor: -1,
    min_damage: 18,
    max_damage: 29,
    main_attribute: HERO_MAIN_ATTRIBUTE.AGILLITY,
    base_attack_time: 1.7,
    base_magic_resistance: 0.25,
    base_movement_speed: 300
  },
  {
    name: "Sniper",
    strength: 16,
    extra_strength_per_level: 1.7,
    agillity: 21,
    extra_agillity_per_level: 2.5,
    intelligence: 15,
    extra_intelligence_per_level: 2.6,
    mana_point: 50,
    mana_regeneration: 0.01,
    health_point: 200,
    health_regeneration: 0.25,
    armor: -1,
    min_damage: 15,
    max_damage: 21,
    main_attribute: HERO_MAIN_ATTRIBUTE.AGILLITY,
    base_attack_time: 1.7,
    base_magic_resistance: 0.25,
    base_movement_speed: 290
  }
];

@Injectable()
export class HeroService {
  public heroes:IHero[] = [];

  constructor() {
    let hero_properties:IHeroProperties[] = this.initHeroes() || HEROES;
    this.heroes = hero_properties.map((prop:IHeroProperties) => new Hero(prop));
  }

  public getHeroes():Promise<IHero[]> {
    return Promise.resolve(this.heroes);
  }

  public getHero(id:number):Promise<IHero> {
    return Promise.resolve(this.heroes.find((hero:IBaseHero) => hero.getId() === id));
  }

  public addHero(new_hero):Promise<void> {
    if (this.heroes.find((hero:IHero) => hero.getName() == new_hero.getName())) {
      return Promise.resolve();
    }

    this.heroes.push(new_hero);
    this.saveHeroes();
  }

  private saveHeroes() {
    window.localStorage.setItem('heroes', this.seriallizeHeroes(this.heroes));
  }

  private initHeroes():IHeroProperties[] {
    return JSON.parse(window.localStorage.getItem('heroes'));
  }

  private seriallizeHeroes(heroes:IHero[]):string {
    return JSON.stringify(heroes.map((hero:IHero) => {
        return hero.getProps();
    }));
  }
}
