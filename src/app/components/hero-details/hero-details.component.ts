import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params} from "@angular/router";
import 'rxjs/add/operator/switchMap';
import {HeroService} from "../../services/heroes.service";
import {IBaseHero} from "../../models/hero/base-hero.model";
import {ItemService} from "../../services/item.service";
import {Item} from "../../models/item.model";

@Component({
  selector: "hero-details",
  template:
    `
      <h2>{{hero?.getName()}}</h2>
      <div *ngIf="hero">
        <ul>
          <li>Level: {{hero.getLevel()}}</li>
          <li>Strength: {{hero.getStrength()}}</li>
          <li>Agillity: {{hero.getAgillity()}}</li>
          <li>Intelligence: {{hero.getIntelligence()}}</li>
          <li>Armor: {{hero.getArmor()}}</li>
          <li>Health: {{hero.getHealthPoint()}}</li>
          <li>Mana: {{hero.getManaPoint()}}</li>
          <li>Damage: {{hero.getDamage()}}</li>
          <li>HP Regeneration: {{hero.getHealthRegeneration()}}</li>
          <li>MP Regeneration: {{hero.getManaRegeneration()}}</li>
          <li>Extra spell damage: {{hero.getExtraSpellDamage()}}</li>
          <li>Attack per second: {{hero.getAttackSpeed()}}</li>
          <li>Movement speed: {{hero.getMovementSpeed()}}</li>
          <li>Magic Resistance: {{hero.getMagicResistance()}}</li>
          <li>Damage per second: {{hero.getDamagePerSecond()}}</li>
          <li>Effective Health(Physical Attack): {{hero.getEffectiveHealthFromPhysicalAttack()}}</li>
          <li>Effective Health(Magic Attack): {{hero.getEffectiveHealthFromMagicAttack()}}</li>
        </ul>
        <div>
          <h3>Уровень</h3>
          <button (click)="upLevel()">+1</button>
          <button (click)="downLevel()">-1</button>
        </div>
        <div>
          <ul>
            <li *ngFor="let item of hero.getItems()">{{item.getName()}}&nbsp;</li>
          </ul>
        </div>
        <div>
          <ul>
            <li *ngFor="let item of items">{{item.getName()}}&nbsp;<button (click)="hero.setItem(item)">+</button><button (click)="hero.removeItem(item.getId())">-</button></li>
          </ul>
        </div>
      </div>
    `
})
export class HeroDetailsComponent implements OnInit {
  hero:IBaseHero;
  items:Item[];

  constructor(
    private router:ActivatedRoute,
    private hero_service:HeroService,
    private item_service:ItemService
  ) {}

  ngOnInit():void {
    this.router.params
      .switchMap((params:Params) => this.hero_service.getHero(+params["id"]))
      .subscribe((hero:IBaseHero) => this.hero = hero);
    this.item_service.getItems().then((items:Item[]) => this.items = items);
  }

  upLevel():void {
    this.hero.setLevel(this.hero.getLevel() + 1);
  }

  downLevel():void {
    this.hero.setLevel(this.hero.getLevel() - 1);
  }
}
