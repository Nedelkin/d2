import {Component} from "@angular/core";
import {IHeroProperties} from "../../../models/hero/hero-properties.model";
import {Hero} from "../../../models/hero/hero.model";
import {HeroService} from "../../../services/heroes.service";
@Component({
  selector: 'create-hero',
  template:
    `
<h1>Create hero</h1>
<form #heroForm="ngForm" style="width: 400px">
  <div style="display: flex; justify-content: space-between">
    <label for="name">Name:</label>
    <input name="name" [(ngModel)]="hero_properties.name" required type="text">
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="strength">Strength:</label>
    <input name="strength" [(ngModel)]="hero_properties.strength" required />
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="extra_strength_per_level">Extra strength per level</label>
    <input name="extra_strength_per_level" required [(ngModel)]="hero_properties.extra_strength_per_level"/>
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="agillity">Agillity:</label>
    <input name="agillity" required [(ngModel)]="hero_properties.agillity" />
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="extra_agillity_per_level">Extra agillity per level</label>
    <input name="extra_agillity_per_level" required [(ngModel)]="hero_properties.extra_agillity_per_level" />
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="intelligence">Intelligence:</label>
    <input name="intelligence" required [(ngModel)]="hero_properties.intelligence" >
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="extra_intelligence_per_level">Extra intelligence per level:</label>
    <input name="extra_intelligence_per_level" required [(ngModel)]="hero_properties.extra_intelligence_per_level" >
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="base_movement_speed">Base movement speed:</label>
    <input name="base_movement_speed" required [(ngModel)]="hero_properties.base_movement_speed" >
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="health_point">Health point:</label>
    <input name="health_point" required [(ngModel)]="hero_properties.health_point" >
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="health_regeneration">Health regeneration:</label>
    <input name="health_regeneration" required [(ngModel)]="hero_properties.health_regeneration">
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="mana_point">Mana point:</label>
    <input name="mana_point" required [(ngModel)]="hero_properties.mana_point" >
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="mana_regeneration">Mana regeneration:</label>
    <input name="mana_regeneration" required [(ngModel)]="hero_properties.mana_regeneration" >
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="armor">Armor:</label>
    <input name="armor" required [(ngModel)]="hero_properties.armor" >
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="base_magic_resistance">Base magic resistance:</label>
    <input name="base_magic_resistance" required [(ngModel)]="hero_properties.base_magic_resistance" >
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="min_damage">Minimal damage:</label>
    <input name="min_damage" required [(ngModel)]="hero_properties.min_damage" >
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="max_damage">Max damage:</label>
    <input name="max_damage" required  [(ngModel)]="hero_properties.max_damage" >
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="base_attack_time">Base attack time:</label>
    <input name="base_attack_time" required [(ngModel)]="hero_properties.base_attack_time" >
  </div>
  <div style="display: flex; justify-content: space-between">
    <label for="main_attribute">Main attribute:</label>
    <select name="main_attribute" [(ngModel)]="main_attribute" required >
      <option value="1">Stength</option>
      <option value="2">Agillity</option>
      <option value="3">Intelligence</option>
    </select>
  </div>
  
  <input type="submit" *ngIf="heroForm.valid" value="Create" (click)="createHero(prop); heroForm.reset()"/>
</form>
`
})
export class CreateHeroComponent {
  hero_properties:IHeroProperties = {
    name:null,
    strength:null,
    extra_strength_per_level:null,
    agillity:null,
    extra_agillity_per_level:null,
    intelligence:null,
    extra_intelligence_per_level:null,
    base_movement_speed:null,
    health_point:null,
    health_regeneration:null,
    mana_point:null,
    mana_regeneration:null,
    armor:null,
    base_magic_resistance:null,
    min_damage:null,
    max_damage:null,
    base_attack_time:null,
    main_attribute:null
  };

  constructor(
    private hero_service:HeroService
  ) {
  }

  createHero() {
    this.hero_service.addHero(new Hero(this.hero_properties));
  }

}
