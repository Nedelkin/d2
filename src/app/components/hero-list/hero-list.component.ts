import {Component, OnInit} from "@angular/core";
import {IBaseHero} from "../../models/hero/base-hero.model";
import {HeroService} from "../../services/heroes.service";
@Component({
  selector: "hero-list",
  template:
    `
      <h2>Hero List</h2>
      <ul>
        <li *ngFor="let hero of heroes">
          <a [routerLink]="['/heroes', hero.getId()]">{{hero.getName()}}</a>
        </li>
      </ul>
    `
})

export class HeroListComponent implements OnInit {
  public heroes:IBaseHero[];

  constructor(private _hero_service:HeroService) {

  }

  ngOnInit():void {
    this._hero_service.getHeroes().then((heroes) => this.heroes = heroes);
  }
}
