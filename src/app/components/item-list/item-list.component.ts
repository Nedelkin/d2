import {Component, OnInit} from "@angular/core";
import {ItemService} from "../../services/item.service";
import {IItem} from "../../models/item.model";

@Component({
  selector: "item-list",
  template:
    `
      <h1>Item list</h1>
      <ul >
        <li *ngFor="let item of items">
          <a [routerLink]="['/items', item.id]">{{item.getName()}}</a>
        </li>
      </ul>
        
    `
})

export class ItemListCompoenent implements OnInit {
  items:IItem[];

  constructor(private item_service:ItemService) {

  }

  ngOnInit():void {
    this.item_service.getItems().then((items:IItem[]) => this.items = items);
  }
}

