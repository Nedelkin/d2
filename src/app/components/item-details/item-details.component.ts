import {Component, OnInit} from "@angular/core";
import {ItemService} from "../../services/item.service";
import 'rxjs/add/operator/switchMap';
import {ActivatedRoute, Params} from "@angular/router";
import {IItem} from "../../models/item.model";
@Component({
  selector: "item-details",
  template:
    `
      <h2>Item details</h2>
      <ul *ngIf="item">
        <li *ngFor="let prop of getProps()">
          {{prop.name}}: {{prop.value}}
        </li>
      </ul>
    `
})
export class ItemDetailsComponent implements OnInit {
  item:IItem;
  constructor(
    private item_service:ItemService,
    private router:ActivatedRoute) {}

  ngOnInit():void {
    this.router.params
      .switchMap((params:Params) => this.item_service.getItem(+params["id"]))
      .subscribe((item:IItem) => this.item = item);
  }

  getProps():any[] {
    let props = [];
    if (this.item.getStrength()) {
      props.push({value:this.item.getStrength(), name:"Strength"});
    }

    if (this.item.getAgillity()) {
      props.push({name:"Agillity", value: this.item.getAgillity()});
    }

    if (this.item.getIntelligence()) {
      props.push({name: "Intelligence", value: this.item.getIntelligence()});
    }

    if (this.item.getArmor()) {
      props.push(({name: "Armor", value: this.item.getArmor()}));
    }
    return props;
  }
}
