import {IHeroProperties, HERO_MAIN_ATTRIBUTE} from "./hero-properties.model";
export interface IBaseHero {
  getId():number
  getName():string;
  getLevel():number;
  getArmor():number;
  getStrength():number;
  getAgillity():number;
  getManaPoint():number;
  getHealthPoint():number;
  getIntelligence():number;
  getDamage():number;
  getMainAttribute():number;
  getHealthRegeneration():number;
  getManaRegeneration():number;
  getExtraSpellDamage():number;
  getAttackSpeed():number;
  getMagicResistance():number;
  getMovementSpeed():number;
  setLevel(level:number):void;
  getProps():IHeroProperties;
}

export class BaseHero implements IBaseHero {

  constructor(properties:IHeroProperties) {
    this.properties = properties;
  }

  public getId():number {
    return this.id;
  }

  public getName():string {
    return this.properties.name;
  }
  public getLevel():number {
    return this.level;
  }
  public getStrength():number {
    return this.properties.strength + this.properties.extra_strength_per_level * (this.getLevel() - 1);
  }
  public getAgillity():number {
    return this.properties.agillity + this.properties.extra_agillity_per_level * (this.getLevel() - 1);
  }
  public getIntelligence():number {
    return this.properties.intelligence + this.properties.extra_intelligence_per_level * (this.getLevel() - 1);
  }
  public getHealthPoint():number {
    return this.properties.health_point + this.getStrength() * BaseHero.HEALTH_PER_LEVEL;
  }
  public getManaPoint():number {
    return this.properties.mana_point + this.getIntelligence() * BaseHero.MANA_PER_LEVEL;
  }
  public getArmor():number {
    return this.properties.armor + this.getAgillity() * BaseHero.ARMOR_PER_LEVEL;
  }
  public getDamage():number {
    return this.getBaseDamage() + this.getMainAttribute();
  }
  public getHealthRegeneration():number {
    return this.properties.health_regeneration + this.getStrength() * BaseHero.HEALTH_REGENERATION_PER_STRENGTH;
  }
  public getManaRegeneration():number {
    return (this.properties.mana_regeneration + this.getIntelligence() * BaseHero.MANA_REGENERATION_INTELLIGENCE);
  }
  public getAttackSpeed():number {
    return (100 + this.getAgillity()) * 0.01 * (1 / this.properties.base_attack_time);
  }

  public getMovementSpeed():number {
    return this.properties.base_movement_speed;
  }

  public getMagicResistance():number {
    return 1 - this.properties.base_magic_resistance;
  }

  public getMainAttribute():number {
    switch (this.properties.main_attribute) {
      case HERO_MAIN_ATTRIBUTE.AGILLITY: return this.getAgillity();
      case HERO_MAIN_ATTRIBUTE.INTELLIGENCE: return this.getIntelligence();
      case HERO_MAIN_ATTRIBUTE.STRENGTH: return this.getStrength();
      default: throw new Error("BaseHero haven't main attribure");
    }
  }

  public setLevel(level:number):void {
    if (level >= 1 && level <= 25) {
      this.level = level;
    }
  }

  public getExtraSpellDamage():number {
    return this.getIntelligence() * BaseHero.EXTRA_SPELL_DAMAGE_PER_INTELLIGENCE;
  }

  public getProps():IHeroProperties {
    return this.properties;
  }

  private getBaseDamage():number {
    return (this.properties.min_damage + this.properties.max_damage)/2;
  }
  private properties:IHeroProperties;
  private id:number = ++BaseHero.id;
  private level:number = 1;

  private static HEALTH_PER_LEVEL:number = 20;
  private static MANA_PER_LEVEL:number = 12;
  private static ARMOR_PER_LEVEL:number = 1/7;
  private static HEALTH_REGENERATION_PER_STRENGTH = 0.03;
  private static MANA_REGENERATION_INTELLIGENCE = 0.04;
  private static EXTRA_SPELL_DAMAGE_PER_INTELLIGENCE = 0.0625;
  private static id:number = 0;
}


