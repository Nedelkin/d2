import {IHeroWithItems, HeroWithItems} from "./hero-with-items";
export interface IHero extends IHeroWithItems {
  getDamagePerSecond():number;
  getEffectiveHealthFromPhysicalAttack():number;
  getEffectiveHealthFromMagicAttack():number;
}

export class Hero extends HeroWithItems implements IHero {
  public getDamagePerSecond():number {
    return this.getDamage() * this.getAttackSpeed();
  }
  public getEffectiveHealthFromPhysicalAttack():number {
    return this.getHealthPoint() / this.getPhysicalDamageMultiplier();
  }
  public getEffectiveHealthFromMagicAttack():number {
    return this.getHealthPoint() / this.getMagicDamageMultiplier();
  }

  private getMagicDamageMultiplier():number {
    return 1 - this.getMagicResistance();
  }

  private getPhysicalDamageMultiplier():number {
    let armor = this.getArmor();
    return 1 - (0.06 * armor) / (1 + (0.06 * Math.abs(armor)));
  }
}
