import {BaseHero, IBaseHero} from "./base-hero.model";
import {IHeroProperties} from "./hero-properties.model";
import {Item, IItem} from "../item.model";

export interface IHeroWithItems extends IBaseHero {
  setItem(item:Item):void;
  removeItem(item_id:number):void;
  getItems():IItem[];
}
export class HeroWithItems extends BaseHero implements IHeroWithItems{
  constructor(
    properties:IHeroProperties,
    private items:IItem[] = []) {
    super(properties);
  }

  public getStrength():number {
    return super.getStrength() + this.getItemsStrength();
  }

  public getAgillity():number {
    return super.getAgillity() + this.getItemsAgillity();
  }

  public getIntelligence():number {
    return super.getIntelligence() + this.getItemsIntelligence();
  }

  public getHealthPoint():number {
    return super.getHealthPoint() + this.getItemsHealthPoint();
  }

  public getManaPoint():number {
    return super.getManaPoint() + this.getItemsManaPoint();
  }
  public getArmor():number {
    return super.getArmor() + this.getItemsArmor();
  }
  public getDamage():number {
    return super.getDamage() + this.getItemsDamage();
  }
  public getHealthRegeneration():number {
    return super.getHealthRegeneration()+ this.getItemsHealthPointRegeneration();
  }
  public getManaRegeneration():number {
    return super.getManaRegeneration() * (1 + this.getItemsManaPointPercentRegeneration()) + this.getItemsManaPointIntegerRegeneration();
  }
  public getAttackSpeed():number {
    return super.getAttackSpeed() + this.getItemsAttackSpeed();
  }

  public getMovementSpeed():number {
    return super.getMovementSpeed() + this.getItemsMoveSpeed();
  }

  public getMagicResistance():number {
    return 1 - super.getMagicResistance() * this.getItemsMagicResistance();
  }

  public setItem(item:IItem):void {
    if (this.items.length < HeroWithItems.MAX_ITEMS)   {
      this.items.push(item);
    }
  }

  public removeItem(item_id:number):void {
    let item = this.items.find((item:Item) => item.getId() == item_id);
    if (item) {
      let index = this.items.indexOf(item);
      this.items.splice(index, 1);
    }
  }

  public getItems():IItem[] {
    return this.items;
  }

  // private

  private getItemsStrength():number {
    return this.items.reduce((strength:number, item:IItem) => strength + item.getStrength(), 0);
  }
  private getItemsAgillity():number {
    return this.items.reduce((agillity:number, item:IItem) => agillity + item.getAgillity(), 0);
  }
  private getItemsIntelligence():number {
    return this.items.reduce((intelligence:number, item:IItem) => intelligence + item.getIntelligence(), 0);
  }
  private getItemsArmor():number {
    return this.items.reduce((armor:number, item:IItem) => armor + item.getArmor(), 0);
  }
  private getItemsDamage():number {
    return this.items.reduce((damage:number, item:IItem) => damage + item.getDamage(), 0);
  }

  private getItemsHealthPoint():number {
    return this.items.reduce((health_point:number, item:IItem) => health_point + item.getHealthPoint(), 0);
  }

  private getItemsManaPoint():number {
    return this.items.reduce((mana_point:number, item:IItem) => mana_point + item.getManaPoint(), 0);
  }

  private getItemsMoveSpeed():number {
    return this.items.reduce((move_speed:number, item:IItem) => move_speed + item.getMoveSpeed(), 0);
  }

  private getItemsAttackSpeed():number {
    return this.items.reduce((attack_speed:number, item:IItem) => attack_speed + item.getAttackSpeed(), 0);
  }

  private getItemsHealthPointRegeneration():number {
    return this.items.reduce((hp_regen:number, item:IItem) => hp_regen + item.getHealthPointRegeneration(), 0);
  }

  private getItemsManaPointPercentRegeneration():number {
    return this.items.reduce((mp_regen:number, item:IItem) => mp_regen + item.getManaPointPercentRegeneration(), 0);
  }
  private getItemsManaPointIntegerRegeneration():number {
    return this.items.reduce((mp_regen:number, item:IItem) => mp_regen + item.getManaPointIntegerRegeneration(), 0);
  }
  private getItemsMagicResistance():number {
    return this.items.reduce((resistance:number, item:IItem) => resistance * (1 - item.getMagicResistance()) , 1)
  }
  private static MAX_ITEMS = 6;
}



