export enum HERO_MAIN_ATTRIBUTE {
  STRENGTH = 1,
  AGILLITY,
  INTELLIGENCE
}

export interface IHeroProperties {
  name:string;
  strength:number;
  extra_strength_per_level:number;
  agillity:number;
  extra_agillity_per_level:number;
  intelligence:number;
  extra_intelligence_per_level:number;
  base_movement_speed:number,
  health_point:number;
  health_regeneration:number;
  mana_point:number;
  mana_regeneration:number;
  armor:number;
  base_magic_resistance:number;
  min_damage:number;
  max_damage:number;

  base_attack_time:number;

  main_attribute:HERO_MAIN_ATTRIBUTE
}
