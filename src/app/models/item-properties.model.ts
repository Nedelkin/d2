export interface IItemProperties {
  name:string;
  strength?:number;
  agillity?:number;
  intelligence?:number;
  price?:number;
  armor?:number;
  damage?:number;
  health_point?:number;
  mana_point?:number;
  move_speed?:number;
  attack_speed?:number;
  health_point_regeneration?:number;
  mana_point_regeneration_percent?:number;
  mana_point_regeneration_integer?: number;
  magic_resistance?:number;
}

