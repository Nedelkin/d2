import {IItemProperties} from "./item-properties.model";

export interface IItem {
  getId():number;
  getName():string;
  getPrice():number;
  getArmor():number;
  getAgillity():number;
  getStrength():number;
  getIntelligence():number;
  getDamage():number;
  getHealthPoint():number;
  getManaPoint():number;
  getMoveSpeed():number;
  getAttackSpeed():number;
  getHealthPointRegeneration():number;
  getManaPointPercentRegeneration():number;
  getManaPointIntegerRegeneration():number;
  getMagicResistance():number;
}

export class Item implements IItem {
  constructor(private props:IItemProperties) {

  }

  public getId():number {
    return this.id;
  }

  public getName():string {
    return this.props.name;
  }

  public getPrice():number {
    return this.props.price || 0;
  }

  public getArmor():number {
    return this.props.armor || 0;
  }

  public getStrength():number {
    return this.props.strength || 0;
  }

  public getAgillity():number {
    return this.props.agillity || 0;
  }

  public getIntelligence():number {
    return this.props.intelligence || 0;
  }

  public getDamage():number {
    return this.props.damage || 0;
  }

  public getHealthPoint():number {
    return this.props.health_point || 0;
  }

  public getManaPoint():number {
    return this.props.mana_point || 0;
  }

  public getMoveSpeed():number {
    return this.props.move_speed || 0;
  }

  public getAttackSpeed():number {
    return this.props.attack_speed || 0;
  }

  public getHealthPointRegeneration():number {
    return this.props.health_point_regeneration;
  }

  public getManaPointPercentRegeneration():number {
    return this.props.mana_point_regeneration_percent;
  }

  public getManaPointIntegerRegeneration():number {
    return this.props.mana_point_regeneration_integer;
  }

  public getMagicResistance():number {
    return this.props.magic_resistance;
  }

  private id:number = ++Item.id;
  private static id:number = 0;
}
