import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {HeroDetailsComponent} from "./components/hero-details/hero-details.component";
import {RouterModule} from "@angular/router";
import {HeroService} from "./services/heroes.service";
import {HeroListComponent} from "./components/hero-list/hero-list.component";
import {ItemDetailsComponent} from "./components/item-details/item-details.component";
import {ItemListCompoenent} from "./components/item-list/item-list.component";
import {ItemService} from "./services/item.service";
import {CreateHeroComponent} from "./components/admin/create-hero/create-hero.component";

@NgModule({
  declarations: [
    AppComponent,
    HeroDetailsComponent,
    HeroListComponent,
    ItemDetailsComponent,
    ItemListCompoenent,
    CreateHeroComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: "heroes/:id",
        component: HeroDetailsComponent
      },{
        path: "heroes",
        component: HeroListComponent
      },{
        path: "items",
        component: ItemListCompoenent
      },{
        path: "items/:id",
        component: ItemDetailsComponent
      },{
        path: "admin/hero/new",
        component: CreateHeroComponent
      }
    ])
  ],
  providers: [
    HeroService,
    ItemService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
